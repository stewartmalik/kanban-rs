import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import App from './App.vue'

Vue.config.productionTip = false;
Vue.use(Vuex);

const TASK_URL = 'http://localhost:8000/api/v1/tasks';

const store = new Vuex.Store({
    state: {
        tasks: [],
        isNewTaskActive: false,
    },

    mutations: {
        SET_TASKS(state, tasks) {
            state.tasks = tasks;
        },

        APPEND_TASK(state, task) {
            state.tasks.push(task);
        },

        UPDATE_STATE(state, update) {
            var new_task_status = update.type;

            // We need to iterate through tasks and update the state of every one we've received
            update.updates.forEach(function(update) {
                var existing = state.tasks.find(task => task.id === update.id);
                existing.task_status = new_task_status;
            });
        },

        NEW_TASK_ACTIVE(state, value) {
            state.isNewTaskActive = value;
        },

        REMOVE_TASK(state, task_id) {
            let index = state.tasks.findIndex(task => task.id === task_id);
            state.tasks.splice(index, 1);
        }
    },

    actions: {
        get_tasks(state) {
            axios.get(TASK_URL).then((response) => {
                state.commit('SET_TASKS', response.data);
            })
        },

        create_task(state, task) {
            axios.post(TASK_URL, task).then((response) => {
                state.commit('APPEND_TASK', response.data);
                state.commit('NEW_TASK_ACTIVE', false);
            })
        },

        delete_task(state, task_id) {
            axios.delete(TASK_URL + '/' + task_id).then( (response) => {
                if (response.status === 200 && response.data.success === true) {
                    state.commit('REMOVE_TASK', task_id);
                }
            })
        }
    }

});

new Vue({
    render: h => h(App),

    store,

    created() {
        this.$store.dispatch('get_tasks')
    }
}).$mount('#app');
