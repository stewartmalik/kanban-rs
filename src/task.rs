use serde::{Serialize, Deserialize};
use uuid::Uuid;
use rocket::request::FromRequest;
use rocket::{Request, Outcome};
use rocket::outcome::Outcome::{Success, Failure};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Task {
    id: Option<Uuid>,

    name: String,

    description: String,

    #[serde(default)]
    date_created: String,

    #[serde(default)]
    date_modified: String,

    #[serde(default)]
    task_status: TaskStatus,
}

impl Task {
    pub fn set_id(&mut self, new_uuid: Uuid ) {
        self.id = Some(new_uuid)
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
enum TaskStatus {
    ToDo,
    Doing,
    Done,
}

impl Default for TaskStatus {
    fn default() -> Self {
        TaskStatus::ToDo
    }
}