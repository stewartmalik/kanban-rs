#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate dotenv_codegen;

use rocket_contrib::json::{Json, JsonValue};
use rocket_contrib::serve::StaticFiles;
use rocket_cors::{AllowedHeaders,AllOrSome};
use rocket::http::{Method,RawStr};
use std::sync::Mutex;
use std::collections::HashMap;
use rocket::State;
use uuid::Uuid;
use serde_json::json;

mod task;

const API_VERSION: &'static str = dotenv!("API_VERSION");

type ID = uuid::Uuid;
type TaskList = Mutex<HashMap<ID, task::Task>>;

#[get("/tasks")]
fn get(task_list: State<TaskList>) -> Json<Vec<task::Task>> {
    // Lock the mutex to prevent access from a different thread
    let task_list = task_list.lock().unwrap();

    // Create Vector for output
    let mut output: Vec<task::Task> = Vec::new();

    // Feed the task list into the output
    for (id, task) in task_list.iter() {
        output.push(task.clone());
    }

    // Return either empty JSON array or items
    Json(output)
}

#[delete("/tasks/<id>")]
fn delete(task_list: State<TaskList>, id: String) -> JsonValue {
    // Lock the mutex to prevent access from a different thread
    let mut task_list = task_list.lock().unwrap();

    // First we need to convert the String to a uuid
    let uuid_parse_attempt = Uuid::parse_str(&id);
    let mut uuid;
    if let Ok(parsed_uuid) = uuid_parse_attempt {
        uuid = parsed_uuid;
    } else {
        return JsonValue(json!({"success": false}));
    }

    // Remove from task_list
    match task_list.contains_key(&uuid) {
        true => {
            task_list.remove(&uuid);
            return JsonValue(json!({"success": true}));
        }
        false => return JsonValue(json!({"success": false}))
    }
}

#[post("/tasks", format = "json", data = "<task>")]
fn new(task_list: State<TaskList>, mut task: Json<task::Task>) -> Json<task::Task> {
    // Lock the mutex to prevent access from a different thread
    let mut task_list = task_list.lock().unwrap();

    // Generate a new UUID for it, based on the SHA1 of the data
    let new_id = Uuid::new_v4();

    // Update the id on the task
    task.0.set_id(new_id);

    let cloned_task = task.clone();

    // insert into task_list
    task_list.entry(new_id).or_insert(task.0);

    Json(cloned_task)
}

fn main() {
    // Set API base
    let base_url = String::from(format!("/api/{}", API_VERSION));

    // We need to setup CORS as well
    let cors = rocket_cors::CorsOptions {
        allowed_origins: AllOrSome::All,
        allowed_methods: vec![Method::Get, Method::Post, Method::Delete].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::all(),
        allow_credentials: true,
        ..Default::default()
    }.to_cors().unwrap();

    rocket::ignite()
        .mount(&base_url, routes![get, new, delete])
        .mount("/", StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"),"/web")))
        .attach(cors)
        .manage(Mutex::new(HashMap::<ID, task::Task>::new()))
        .launch();
}